/*
 * udma_chan.c
 * This file provides udma channel functionality to user space applications
 * to use the udma kernel driver.
 *
 * Copyright (C) 2012 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#pragma GCC diagnostic ignored "-Wsign-compare"
#include <internal/udma_internal.h>

static int udma_align		= 64;

struct udma_chan {
	void			*mem;
	unsigned		 size, num_desc;
	struct udma_chan_data	 data;
	struct vring		 vring;
	struct udma_desc_data	*desc_data;

	struct udma_atom	 last_used_idx;
	struct udma_atom	 num_free;
	struct udma_atom	 free_head;
};

struct udma_desc_data {
	void			*handle;
	udma_chan_callback_t	 cb;
	void			*buf;
	unsigned		 size;
};

int udma_chan_get_size(int num_desc)
{
	return (sizeof(struct udma_chan) +
		sizeof(struct udma_desc_data) * num_desc);
}

struct udma_chan *udma_chan_create(const char *name, enum udma_chan_dir dir,
				   int num_desc)
{
	struct udma_chan_data *data;
	struct udma_chan *chan;
	struct vring *vring;
	unsigned size;
	void *mem;
	int ret, i;

	/* descriptors must be a power of two */
	if (num_desc & (num_desc - 1)) {
		fprintf(stderr, "number of descriptors not a power of two\n");
		return NULL;
	}

	chan = udma_malloc(udma_chan_get_size(num_desc));
	if (!chan) {
		fprintf(stderr, "failed to allocate channel\n");
		return NULL;
	}

	if (dir != UDMA_CHAN_TX && dir != UDMA_CHAN_RX) {
		fprintf(stderr, "bad channel direction\n");
		return NULL;
	}
	udma_lock();

	memset(chan, 0, udma_chan_get_size(num_desc));

	chan->desc_data = (struct udma_desc_data*)(chan + 1);
	chan->num_desc  = num_desc;

	size = vring_size(num_desc, udma_align);
	mem = udma_mem_alloc(size);
	if (!mem) {
		udma_free(chan);
		udma_unlock();
		fprintf(stderr, "failed to allocate mem\n");
		return NULL;
	}

	vring = &chan->vring;
	data = &chan->data;

	data->num_desc	= num_desc;
	data->align	= udma_align;
	data->ring_virt	= (unsigned long)mem;
	data->ring_size	= size;
	data->direction	= (dir == UDMA_CHAN_TX) ? DMA_MEM_TO_DEV :
						  DMA_DEV_TO_MEM;
	udma_atom_set(&chan->num_free, num_desc);
	udma_atom_set(&chan->free_head, 0);
	strncpy(data->name, name, sizeof(data->name) - 1);

	vring_init(&chan->vring, num_desc, mem, udma_align);

	ret = ioctl(udma_fd, UDMA_IOC_ATTACH, data);
	if (ret < 0) {
		udma_mem_free(mem, size);
		udma_free(chan);
		udma_unlock();
		fprintf(stderr, "failed to attach\n");
		return NULL;
	}

	for (i = 0; i < num_desc; i++)
		vring->desc[i].next	= i + 1;

	chan->mem = mem;
	chan->size = size;

	udma_unlock();

	return chan;
}

void udma_chan_destroy(struct udma_chan *chan)
{
	close(chan->data.handle);
	udma_mem_free(chan->mem, chan->size);
	udma_free(chan);
}

int udma_chan_get_fd(struct udma_chan *chan)
{
	return(chan->data.handle);
}

void udma_chan_kick(struct udma_chan *chan)
{
	struct vring *vring = &chan->vring;
	struct timespec timeo = {0, 0};
	fd_set	rfds, wrfds, efds;
	int retval;

	udma_print_debug("kick (%s) begin - avail %d, used %d\n",
		   chan->data.name, vring->avail->idx,
		   vring->used->idx);

	FD_ZERO(&rfds);
	FD_SET(chan->data.handle, &rfds);
	FD_ZERO(&wrfds);
	FD_SET(chan->data.handle, &wrfds);
	FD_ZERO(&efds);
	FD_SET(chan->data.handle, &efds);

	retval = pselect(chan->data.handle + 1, &rfds,
			&wrfds, &efds, &timeo, NULL);
	if (retval == -1)
		udma_print_error("kick (%s) select failed ret= %d\n",
				 chan->data.name, retval);
	else if (retval)
		udma_print_debug("kick (%s) end - avail %d, used %d\n",
			   chan->data.name, vring->avail->idx,
			   vring->used->idx);
	else
		udma_print_debug("kick (%s) no data after timeout, ret"
				 "= %d\n", chan->data.name, retval);

}

void udma_chan_poll(struct udma_chan *chan, int budget, struct timespec *timeo)
{
	struct vring *vring = &chan->vring;
	enum udma_atom_status status;
	struct udma_desc_data *data;
	int id, head, retval;
	uint32_t size;
	unsigned short idx;
	void *handle, *buf;
	fd_set	rfds, wrfds, efds;

	udma_print_debug("poll (%s) begin - avail %d, used %d\n",
		   chan->data.name, vring->avail->idx,
		   vring->used->idx);

	/* Right now, nothing to read, just poll
	ret = read(chan->data.handle, &packets, sizeof(int));
	udma_print_debug("first read, packets = %d\n", packets);
	udma_print_debug("poll (%s) avail %d, used %d\n",
		   chan->data.name, vring->avail->idx,
		   vring->used->idx);
	 */
	for (;;) {
		if (unlikely(budget == 0))
			break;

		idx = udma_atom_get(&chan->last_used_idx);
		udma_print_debug("poll (%s) idx %d\n",
		   chan->data.name, idx);

		/* do we have stuff to do in user-space? */
		if (unlikely(idx == vring->used->idx)) {
			FD_ZERO(&rfds);
			FD_SET(chan->data.handle, &rfds);
			FD_ZERO(&wrfds);
			FD_SET(chan->data.handle, &wrfds);
			FD_ZERO(&efds);
			FD_SET(chan->data.handle, &efds);

			retval = pselect(chan->data.handle + 1, &rfds,
					&wrfds, &efds, timeo, NULL);
			if (retval == -1) {
				udma_print_error("select failed ret= %d\n",
						 retval);
				break;
			}
			else if (retval)
				udma_print_debug("data available (%s) ret = %d\n",
						 chan->data.name, retval);
			else {
				udma_print_debug(" no data after timeout, ret"
						 "= %d\n", retval);
				break;
			}
			continue;
		}

		status = udma_atom_cas(&chan->last_used_idx, idx,
				       ((idx + 1) & 0xffff));

		if (likely(status == UDMA_ATOM_CAS_SUCCESS))
			vring_used_event(&chan->vring) = ((idx +1) & 0xffff);
		else if (unlikely(status != UDMA_ATOM_CAS_SUCCESS))
			continue;

		udma_print_verbose("poll (%s) process %d - avail %d, used %d\n",
			   chan->data.name, idx, vring->avail->idx,
			   vring->used->idx);

		idx	&= chan->num_desc - 1;
		id	 = vring->used->ring[idx].id;
		size	 = vring->used->ring[idx].len;
		data	 = chan->desc_data + id;
		handle	 = data->handle;
		buf	 = data->buf;

		do {
			head = udma_atom_get(&chan->free_head);
			vring->desc[id].next = head;
			status = udma_atom_cas(&chan->free_head, head, id);
		} while (status != UDMA_ATOM_CAS_SUCCESS);
		udma_atom_inc(&chan->num_free);
		barrier();

		udma_print_verbose("poll (%s) pre-cb - avail %d, used %d\n",
			   chan->data.name, vring->avail->idx,
			   vring->used->idx);

		(*data->cb)(chan, handle, buf, size);

		if (likely(budget > 0))
			budget--;

		udma_print_verbose("poll (%s) post-cb - avail %d, used %d\n",
			   chan->data.name, vring->avail->idx,
			   vring->used->idx);
	}

	udma_print_debug("poll (%s) end - avail %d, used %d\n",
		   chan->data.name, vring->avail->idx,
		   vring->used->idx);
}

int udma_chan_submit(struct udma_chan *chan, void *handle,
		     udma_chan_callback_t cb, void *buf, unsigned size)
{
	struct vring		*vring = &chan->vring;
	unsigned int		idx;
	int			head, next;
	enum udma_atom_status	status;
	struct timespec		timeo = { 0, 0 };

	udma_print_debug("submit (%s) begin - avail %d, used %d\n",
		   chan->data.name, vring->avail->idx,
		   vring->used->idx);

	/* get desc from free list */
	if (!udma_atom_get(&chan->num_free)) {
		udma_chan_poll(chan, 1, &timeo);
	}

	if (udma_atom_dec(&chan->num_free) < 0) {
		udma_atom_inc(&chan->num_free);
		return -ENOMEM;
	}

	do {
		head = udma_atom_get(&chan->free_head);
		next = vring->desc[head].next;
		status = udma_atom_cas(&chan->free_head, head, next);
	} while (status != UDMA_ATOM_CAS_SUCCESS);

	/* save user's context info */
	chan->desc_data[head].handle	= handle;
	chan->desc_data[head].cb	= cb;
	chan->desc_data[head].buf	= buf;
	chan->desc_data[head].size	= size;
	vring->desc[head].addr		= (uint64_t)((uint32_t)buf);
	vring->desc[head].len		= size;
	barrier();

	/* put desc into avail list */
	idx = vring->avail->idx;
	vring->avail->ring[idx & (chan->num_desc - 1)] = head;
	vring->avail->idx++;

	udma_print_debug("submit (%s) end - avail %d, used %d, ring idx %d, "
			 "desc idx %d\n",
			 chan->data.name, vring->avail->idx,
			 vring->used->idx, idx, head);

	return 0;
}
