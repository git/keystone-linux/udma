/*
 * udma.h
 *
 * Architecture specific (ARM9) routines for atomic operations
 *
 * Copyright (C) 2012 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#ifndef __UDMA_ATOM_ARCH_ARM__
#define __UDMA_ATOM_ARCH_ARM__

#define LINUX_USER_HELPER_CMPXCHG   0xffff0fc0

/*
 * @brief   Kernel user mode cmpxchg helper routine type.
 *          See arch/arm/kernel/entry-armv.S in the kernel
 *          source for further details.
 */
typedef int (__kernel_cmpxchg_t)(int oldval, int newval, volatile int *ptr);

#define __kernel_cmpxchg (*(__kernel_cmpxchg_t *)(LINUX_USER_HELPER_CMPXCHG))

/**
 * @brief       Perform an atomic compare-and-set operation
 *
 * @param[in]   v       Atomic object on which to operate.
 * @param[in]   cmp     Value to compare object against.
 * @param[in]   val     Value to set object to if comparison
 *                      holds true.
 *
 * @return UDMA_ATOM_CAS_FAILED      CAS operation failed because
 *                                  object value did not match
 *                                  the compare value.
 *
 * @return UDMA_ATOM_CAS_SUCCESS     CAS operation succeeded, the
 *                                  object has been modified.
 *
 * <B>Detailed Description:</B>
 *
 * This routine implements a compare-and-set atomic primitive
 * using processor specific and linux specific constructs.
 *
 * In the case of ARM9, since ldrex/strex instructions are
 * not present in most common pre-ARMV processors, the kernel
 * provides user-mode helper routines at a predefined address.
 * These user-mode routines were provided primarily for NPTL
 * use, but works out quite conveniently for our purposes as
 * well.
 */
static inline enum udma_atom_status
udma_atom_cas(struct udma_atom *v, int cmp, int val)
{
	return __kernel_cmpxchg(cmp, val, &v->counter) ?
		UDMA_ATOM_CAS_FAILED : UDMA_ATOM_CAS_SUCCESS;
}

#endif /* __UDMA_ATOM_ARCH_ARM__ */
