/*
 * udma_atom.h
 *
 * UDMA uses processor specific atomic primitives to avoid system
 * calls and locks in many situations.
 *
 * For instance, LIFO list primitives built upon atomic compare-and-set
 * operations serve as the primary data structure used in the memory
 * management facility.
 *
 * This file contains architecture independent primitives (such as
 * the above mentioned LIFO list) that rely on an architecture dependent
 * implementation of compare-and-set. These architecture specific
 * implementations can be found in arch/ARCH/udma_atom_arch.h.
 *
 *
 * Copyright (C) 2012 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#ifndef __UDMA_ATOM__H__
#define __UDMA_ATOM__H__

/** An atomic object structure */
struct udma_atom
{
	volatile int counter; /**< The actual value */
};

/** A lock-free LIFO list type */
struct udma_lifo
{
	struct udma_atom head; /**< The head of the list */
};

/** A lock-free LIFO list node structure */
struct udma_lifo_node
{
	struct udma_atom next; /**< the next node in the list */
};

enum udma_atom_status
{
	/** Compare-and-set operation succeeded, the atomic object has been
	 * modified */
	UDMA_ATOM_CAS_SUCCESS = 0,

	/** Compare-and-set operation failed because of interruption, the
	 * operation should be retried */
	UDMA_ATOM_CAS_RETRY,

	/** Compare-and-set operation failed because the object did not match
	 * the compare value */
	UDMA_ATOM_CAS_FAILED
};

#if defined(ARCH_arm)
#include <internal/udma_atom_arm.h>
#else
#error "unknown arch"
#endif

/**
 * @brief   Static initializer for an atomic object with a default value of "x"
 */
#define UDMA_ATOM_INIT(x)        { (int)(x) }

/**
 * @brief   Static initializer for an empty atomic
 *          LIFO list.
 */
#define UDMA_LIFO_INIT           { UDMA_ATOM_INIT(NULL) }

/**
 * @brief       Get the integer value of an atomic object.
 * @param[in]   object  Atomic object whose value is to be fetched.
 * @return      Retreived value of the object.
 */
static inline int udma_atom_get(struct udma_atom *object)
{
	return object->counter;
}

/**
 * @brief       Get the pointer value of an atomic object.
 * @param[in]   object  Atomic object whose value is to be fetched.
 * @return      Retreived value of the object.
 */
static inline void *udma_atom_get_ptr(struct udma_atom *object)
{
	return (void*)udma_atom_get(object);
}

/**
 * @brief       Set an atomic object to a specified integer value
 * @param[in]   object  Pointer to the atomic object to be set.
 * @param[in]   value   Value to set the object to.
 * @return      none
 * @see         udma_atom_cas
 *
 * <B>Detailed Description:</B>
 *
 * This routine unconditionally sets an atomic object to a specified value. To
 * conditionally set the object, use udma_atom_cas() instead.
 */
static inline void udma_atom_set(struct udma_atom *object, int value)
{
	object->counter = value;
}

/**
 * @brief       Set an atomic object to a specified pointer value
 * @param[in]   object  Pointer to the atomic object to be set.
 * @param[in]   value   Value to set the object to.
 * @return      none
 * @see         udma_atom_cas
 *
 * <B>Detailed Description:</B>
 *
 * This routine unconditionally sets an atomic object to a specified value. To
 * conditionally set the object, use udma_atom_cas() instead.
 */
static inline void udma_atom_set_ptr(struct udma_atom *object, void *value)
{
	udma_atom_set(object, (int)value);
}

/**
 * @brief       Wrapper around udma_atom_cas for pointer data.
 * @param[in]   v       Atomic object on which to operate.
 * @param[in]   cmp     Value to compare object against.
 * @param[in]   val     Value to set object to if comparison
 *                      holds true.
 *
 * @return UDMA_ATOM_CAS_FAILED      CAS operation failed because
 *                                  object value did not match
 *                                  the compare value.
 *
 * @return UDMA_ATOM_CAS_RETRY       CAS operation failed because
 *                                  the operation was interrupted,
 *                                  but may succeed if retried.
 *
 * @return UDMA_ATOM_CAS_SUCCESS     CAS operation succeeded, the
 *                                  object has been modified.
 *
 * <B>Detailed Description:</B>
 *
 * This routine provides a simple wrapper around udma_atom_cas for
 * pointer typed data.
 */
static inline enum udma_atom_status
udma_atom_cas_ptr(struct udma_atom *v, void *cmp, void *val)
{
	return udma_atom_cas(v, (int)cmp, (int)val);
}

/**
 * @brief       Dynamic initializer for an atomic object.
 * @param[in]   object  Atomic object to be initialized.
 * @param[in]   defval  Default value.
 * @return  none
 */
static inline void udma_atom_init(struct udma_atom *object, int defval)
{
	udma_atom_set(object, defval);
}

/**
 * @brief       Dynamic initializer for an atomic object.
 * @param[in]   object  Atomic object to be initialized.
 * @param[in]   defval  Default value.
 * @return  none
 */
static inline void udma_atom_init_ptr(struct udma_atom *object, void *defval)
{
	udma_atom_set_ptr(object, defval);
}

/**
 * @brief       Atomically set an object and return the original value.
 * @param[in]   object  Atomic object to be modified.
 * @param[in]   value   Value to set object to
 * @return  The original value.
 */
static inline int udma_atom_xchg(struct udma_atom *object, int newval)
{
	enum udma_atom_status status;
	int oldval;

	do {
		oldval = udma_atom_get(object);
		status = udma_atom_cas(object, oldval, newval);
	} while (status != UDMA_ATOM_CAS_SUCCESS);

	return oldval;
}

/**
 * @brief       Atomically set an object pointer and return the original value.
 * @param[in]   object  Atomic object to be modified.
 * @param[in]   value   Value to set object to
 * @return  The original value.
 */
static inline void *udma_atom_xchg_ptr(struct udma_atom *object, void *newval)
{
	enum udma_atom_status status;
	void *oldval;

	do {
		oldval = udma_atom_get_ptr(object);
		status = udma_atom_cas_ptr(object, oldval, newval);
	} while (status != UDMA_ATOM_CAS_SUCCESS);

	return oldval;
}

/**
 * @brief       Atomically set a bit in an object and return the original
 *              bit value.
 * @param[in]   object  Atomic object to be modified.
 * @param[in]   bit     Bit number (0 <= i < 32).
 * @return  The original bit.
 */
static inline int udma_atom_set_bit(struct udma_atom *object, int bit)
{
	enum udma_atom_status status;
	int mask = (1 << bit);
	int oldval, newval;

	do {
		oldval = udma_atom_get(object);
		if (oldval & mask)
			return 1;
		newval = oldval | mask;
		status = udma_atom_cas(object, oldval, newval);
	} while (status != UDMA_ATOM_CAS_SUCCESS);
	return 0;
}

/**
 * @brief       Atomically clear a bit in an object and return the original
 *              bit value.
 * @param[in]   object  Atomic object to be modified.
 * @param[in]   bit     Bit number (0 <= i < 32).
 * @return  The original bit.
 */
static inline int udma_atom_clear_bit(struct udma_atom *object, int bit)
{
	enum udma_atom_status status;
	int mask = (1 << bit);
	int oldval, newval;

	do {
		oldval = udma_atom_get(object);
		if (!(oldval & mask))
			return 0;
		newval = oldval & (~mask);
		status = udma_atom_cas(object, oldval, newval);
	} while (status != UDMA_ATOM_CAS_SUCCESS);
	return 1;
}

/**
 * @brief       Atomically add a constant value to the contents of an
 *              atomic object.
 * @param[in]   object  Atomic object to be modified.
 * @param[in]   i       Increment value.
 * @return  The modified result.
 */
static inline int udma_atom_add(struct udma_atom *object, int i)
{
	enum udma_atom_status status;
	int oldval, newval;
	do {
		oldval = udma_atom_get(object);
		newval = oldval + i;
		status = udma_atom_cas(object, oldval, newval);
	} while (status != UDMA_ATOM_CAS_SUCCESS);
	return newval;
}

/**
 * @brief       Atomically subtract a constant value to the
 *              contents of an atomic object.
 * @param[in]   object  Atomic object to be modified.
 * @param[in]   i       Decrement value.
 * @return  The modified result.
 */
static inline int udma_atom_sub(struct udma_atom *object, int i)
{
	return udma_atom_add(object, -i);
}

/**
 * @brief       Atomically increment an atomic object.
 * @param[in]   object  Atomic object to be modified.
 * @return  The modified result.
 */
static inline int udma_atom_inc(struct udma_atom *object)
{
	return udma_atom_add(object, 1);
}

/**
 * @brief       Atomically decrement an atomic object.
 * @param[in]   object  Atomic object to be modified.
 * @return  The modified result.
 */
static inline int udma_atom_dec(struct udma_atom *object)
{
	return udma_atom_sub(object, 1);
}

/**
 * @brief       Dynamic initializer for an lock-free LIFO list.
 * @param[in]   lifo    List object to be initialized.
 * @return  none
 */
static inline void udma_lifo_init(struct udma_lifo *lifo)
{
	udma_atom_set_ptr(&lifo->head, NULL);
}

/**
 * @brief       Insert a new node at the head of the LIFO list.
 * @param[in]   lifo    List object to be manipulated.
 * @param[in]   node    New node to be inserted.
 * @return  none
 */
static inline void udma_lifo_put(struct udma_lifo *lifo,
				 struct udma_lifo_node *node)
{
	enum udma_atom_status status;
	struct udma_lifo_node *head;

	do {
		head = udma_atom_get_ptr(&lifo->head);
		udma_atom_set_ptr(&node->next, head);
		status = udma_atom_cas_ptr(&lifo->head, head, node);
	} while (status != UDMA_ATOM_CAS_SUCCESS);
}

/**
 * @brief       Delete and return the head node of a LIFO list.
 * @param[in]   lifo    List object to be manipulated.
 * @return  node    Extracted node.
 */
static inline struct udma_lifo_node *udma_lifo_get(struct udma_lifo *lifo)
{
	enum udma_atom_status status;
	struct udma_lifo_node * head, * next;

	do {
		head = udma_atom_get_ptr(&lifo->head);
		if (!head)
			break;
		next = udma_atom_get_ptr(&head->next);
		status = udma_atom_cas_ptr(&lifo->head, head, next);
	} while (status != UDMA_ATOM_CAS_SUCCESS);
	return head;
}

#endif /* __UDMA_ATOM__H__ */
