/*
 * udma.h
 *
 * This file provides internal functionality to user space applications
 * to use the udma kernel driver.
 *
 *
 * Copyright (C) 2012 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#ifndef __UDMA_INTERNAL__H__
#define __UDMA_INTERNAL__H__

#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>

#include <sys/ioctl.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/eventfd.h>
#include <sys/mman.h>

#include <udma.h>

#include <internal/linux_udma.h>
#include <internal/linux_vring.h>
#include <internal/udma_atom.h>

#define barrier() __asm__ __volatile__("": : :"memory")

#define UDMA_DEV_NAME		"/dev/udma0"
#define UDMA_DBG_DEV_NAME	"/dev/kmsg"

extern int			 udma_fd;
extern FILE			*udma_dbg_fd;
extern enum udma_dbg_level	 udma_dbg_level;
extern struct udma_osal		*udma_osal;

#define __udma_print(level, ...)					\
	do {							\
		if ((level) >= udma_dbg_level) {		\
			fprintf(udma_dbg_fd, __VA_ARGS__);	\
			fflush(udma_dbg_fd);			\
		}						\
	} while (0)

#define udma_print_verbose(...)	__udma_print(udma_dbg_verbose,	__VA_ARGS__)
#define udma_print_debug(...)	__udma_print(udma_dbg_debug,	__VA_ARGS__)
#define udma_print_info(...)	__udma_print(udma_dbg_info,	__VA_ARGS__)
#define udma_print_warning(...)	__udma_print(udma_dbg_warning,	__VA_ARGS__)
#define udma_print_error(...)	__udma_print(udma_dbg_error,	__VA_ARGS__)

#define likely(x)	__builtin_expect(!!(x), 1)
#define unlikely(x)	__builtin_expect(!!(x), 0)
static inline void udma_lock(void)
{
	if (udma_osal && udma_osal->lock)
		(*udma_osal->lock)();
}

static inline void udma_unlock(void)
{
	if (udma_osal && udma_osal->unlock)
		(*udma_osal->unlock)();
}

static inline void *udma_malloc(unsigned size)
{
	if (udma_osal && udma_osal->malloc)
		return (*udma_osal->malloc)(size);
	else
		return malloc(size);
}

static inline void udma_free(void *data)
{
	if (udma_osal && udma_osal->free)
		(*udma_osal->free)(data);
	else
		free(data);
}

struct udma_mem_part {
	unsigned		 blocksize;
	void			*memstart, *memend;
	struct udma_atom	 used;
	struct udma_lifo	 free;
};

#endif
