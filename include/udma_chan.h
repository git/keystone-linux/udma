/*
 * udma_chan.h
 * This file provides udma channel functionality to user space applications
 * to use the udma kernel driver.
 *
 * Copyright (C) 2012 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#ifndef __UDMA_CHAN__H__
#define __UDMA_CHAN__H__

#ifdef __cplusplus
extern "C" {
#endif

#include <time.h>

struct udma_chan;

enum udma_chan_dir {
	UDMA_CHAN_TX,
	UDMA_CHAN_RX
};

typedef void (*udma_chan_callback_t)(struct udma_chan *chan, void *handle,
				     void *buf, unsigned size);

struct udma_chan *udma_chan_create(const char *name, enum udma_chan_dir dir,
				   int num_desc);
void udma_chan_destroy(struct udma_chan *chan);
int  udma_chan_submit(struct udma_chan *chan, void *handle,
		      udma_chan_callback_t cb,
		      void *buf, unsigned size);
void udma_chan_poll(struct udma_chan *chan, int budget,
		    struct timespec *timeout);
void udma_chan_kick(struct udma_chan *chan);
int udma_chan_get_fd(struct udma_chan *chan);

#ifdef __cplusplus
}
#endif
#endif /* __UDMA_CHAN__H__ */
