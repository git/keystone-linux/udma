/*
 * udma_mem.h
 *
 * UDMA Memory management provides the ability to manage pre-allocated
 * fixed-size pools of buffers through its segment/partition construct.
 *
 * Partitions are then created out of a UDMA segment, with each partition
 * consisting of a number of fixed-size blocks that can be allocated
 * from and returned to the partition with very little overhead.
 *
 * This memory manager implementation relies on an atomic LIFO construct to
 * manage partitions without resorting to system calls or expensive heap
 * operations.
 *
 * Copyright (C) 2012 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#ifndef __UDMA_MEM__H__
#define __UDMA_MEM__H__

#ifdef __cplusplus
extern "C" {
#endif

struct udma_mem_part;

int   udma_mem_init(unsigned long mem_size);
void  udma_mem_shutdown(void);

void *udma_mem_alloc(unsigned long size);
int   udma_mem_free(void *mem, unsigned long size);

struct udma_mem_part *
udma_mem_part_create(unsigned nblocks, unsigned blocksize);
void udma_mem_part_destroy(struct udma_mem_part *part);

void *udma_mem_block_alloc(struct udma_mem_part *part);
void  udma_mem_block_free(struct udma_mem_part *part, void* block);

#ifdef __cplusplus
}
#endif
#endif /* __UDMA_MEM__H__ */
