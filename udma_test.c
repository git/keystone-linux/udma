/*
 * udma_test.c
 * This file provides sample udma test code to user space applications
 * to use the udma kernel driver.
 *
 * Copyright (C) 2012 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <udma.h>

#include <sys/time.h>
#include <sys/types.h>

#include <internal/udma_internal.h>

//#define NUM_PACKETS		64
#define NUM_PACKETS		(2 * 1000 * 10 )
#define BUF_SIZE		128
#define NUM_DESC_PER_CHAN	16
#define NUM_CHAN		1

struct udma_user_info {
	int buf_size;
	int test_id;
	int offset;
	int num_descs;
	int num_packets;
	unsigned long tx_complete;
	unsigned long rx_complete;
};

struct udma_mem_part *part = NULL;
struct udma_chan *txchan, *rxchan;
struct udma_chan *txarr[NUM_CHAN], *rxarr[NUM_CHAN];

struct udma_user_info udma_tcb;

void tx_complete(struct udma_chan *chan, void *handle, void *buf, unsigned size)
{
	udma_print_debug("completed tx packet %p, sent %d, size %d\n",
			 buf, *((unsigned long*)buf), size);
	udma_tcb.tx_complete++;
	udma_mem_block_free(part, buf);
}

void rx_complete(struct udma_chan *chan, void *handle, void *buf, unsigned size)
{
	int ret;

	udma_print_debug("completed rx packet %p, received %d, size %d\n",
			buf, *((unsigned long*)buf), size);
	udma_tcb.rx_complete++;
	ret = udma_chan_submit(rxchan, handle, rx_complete, buf, udma_tcb.buf_size);
	if (ret < 0)
		fprintf(stderr, "rx_complete: error submitting rx packet\n");
}
void udma_print_usage()
{
	udma_print_info("UDMA Test Application\n");
	udma_print_info("Usage\n");
	udma_print_info("udma_test id [test number] [buf size] [offset]\n");
}

void udma_parse_params(int argc, char *argv[])
{
	if (argc > 1)
		udma_tcb.buf_size = atoi(argv[1]);
	if (argc > 2)
		udma_tcb.offset = atoi(argv[2]);

}

int main(int argc, char *argv[])
{
	unsigned long mem_size = 8 * 1024 * 1024;
	unsigned long tx_packets, loop;
	int ret, i;
	char *buf;
	pthread_t thread1, thread2;
	struct timeval start, end;
	unsigned long long usecs;
	struct timespec timeout = { 0, 0 };
	struct timespec timeout1 = { 0, 0 };
	char rxstr[10], txstr[10];


	udma_init(NULL);
	udma_print_info("*****Starting test, UDMA Init\n");
	memset(&udma_tcb, 0, sizeof(udma_tcb));

	udma_parse_params(argc, argv);

	/* Set defaults for test-1 if not already set */
	if (!udma_tcb.buf_size)
		udma_tcb.buf_size = BUF_SIZE;
	if (!udma_tcb.num_descs)
		udma_tcb.num_descs = NUM_DESC_PER_CHAN;
	udma_print_info("buf_size = %d, offset = %d descs = %d\n",
				udma_tcb.buf_size,
				udma_tcb.offset, udma_tcb.num_descs);

	udma_mem_init(mem_size);
	part = udma_mem_part_create(NUM_CHAN*(udma_tcb.num_descs * 2 + 16), udma_tcb.buf_size);

	for (loop = 0; loop < NUM_CHAN; loop++)
	{
		tx_packets = 0;
		udma_tcb.tx_complete = 0;
		udma_tcb.rx_complete = 0;
		part = udma_mem_part_create((udma_tcb.num_descs * 2 + 16), udma_tcb.buf_size);

		snprintf(txstr, sizeof(txstr), "%s%d", "udmatx", loop);
		snprintf(rxstr, sizeof(rxstr), "%s%d", "udmarx", loop);
		udma_print_info("*****Creating %s and %s channels\n", txstr, rxstr);
		txarr[loop] = udma_chan_create(txstr, UDMA_CHAN_TX,
					  udma_tcb.num_descs);
		rxarr[loop] = udma_chan_create(rxstr, UDMA_CHAN_RX,
					  udma_tcb.num_descs);
		txchan = txarr[loop];
		rxchan = rxarr[loop];

		if (!txchan || !rxchan) {
			fprintf(stderr, "error opening udma channels %d\n", loop);
			return 1;
		}
		udma_print_info("*****Running test on %s and %s channels\n", txstr, rxstr);
		ret = udma_chan_get_fd(txchan);
		udma_print_debug("get_fd for tx  returned %d\n", ret);

		ret = udma_chan_get_fd(rxchan);
		udma_print_debug("get_fd for rx  returned %d\n", ret);

		gettimeofday(&start, NULL);

		/* fill rx channel */
		for (i = 0; i < udma_tcb.num_descs; i++) {
			buf = udma_mem_block_alloc(part);
			ret = udma_chan_submit(rxchan, part, rx_complete, buf,
					       udma_tcb.buf_size);
			udma_chan_poll(rxchan, -1, &timeout);
			if (ret < 0) {
				fprintf(stderr, "error submitting rx packet\n");
				continue;
			}
		}

		for (i = 0; i < NUM_PACKETS; i++) {
			do {
				buf = udma_mem_block_alloc(part);
				if (buf)
					break;
				udma_print_info("user space udma: did not get buf\n");
				udma_chan_poll(txchan, -1, &timeout);
				udma_chan_poll(rxchan, -1, &timeout);
			} while (!buf);

			*((unsigned long*)buf) = i;
			ret = udma_chan_submit(txchan, part, tx_complete, buf,
					       udma_tcb.buf_size);
			if (ret < 0) {
				udma_print_info("error submitting pkt #%lu, ret = %d\n",
						i, ret);
				i--;
				udma_mem_block_free(part, buf);
				continue;
			}
			tx_packets++;
			udma_print_debug("submitted tx packet %p, send %d\n", buf, i);
			udma_chan_poll(txchan, -1, &timeout1);
			udma_chan_poll(rxchan, -1, &timeout1);
		}

		gettimeofday(&end, NULL);

		usecs  = (end.tv_sec - start.tv_sec) * 1000000ULL;
		usecs += end.tv_usec;
		usecs -= start.tv_usec;

		if (NUM_PACKETS >= 1000)
			udma_print_info("total time = %llu usecs, %llu nsecs/packet\n",
					usecs, usecs / (NUM_PACKETS / 1000));

		timeout.tv_sec = 0;
		timeout.tv_nsec = 1000;
		for (i = 0; i < 1000; i++) {
			if (udma_tcb.tx_complete == tx_packets &&
			    udma_tcb.rx_complete == tx_packets)
				break;
			udma_chan_poll(txchan, -1, &timeout);
			udma_chan_poll(rxchan, -1, &timeout);
		}

		udma_print_info("Total-packets=%d tx-complete = %lu, rx-complete = %lu\n",
				NUM_PACKETS, udma_tcb.tx_complete, udma_tcb.rx_complete);
		udma_mem_part_destroy(part);
	}
	for (loop = 0; loop < NUM_CHAN; loop++)
	{
		udma_print_info("*****Deleting channel %s-%d %s-%d\n", 
				"udmatx", loop, "udmarx", loop);
		udma_chan_destroy(rxarr[loop]);
		udma_chan_destroy(txarr[loop]);
	}
	return;

}
