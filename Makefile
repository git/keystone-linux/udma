ARCH?=arm
CROSS_COMPILE?=arm-none-linux-gnueabi-

CC?=$(CROSS_COMPILE)gcc
AR?=$(CROSS_COMPILE)ar

ARFLAGS?=crus
LDFLAGS?= -lpthread -Wl,--hash-style=gnu
CFLAGS?=				\
       -march=armv7-a -mtune=cortex-a8	\
       -mthumb-interwork -mno-thumb

EXTRA_CFLAGS=				\
       -Os -Iinclude -I.		\
       -D_GNU_SOURCE -DARCH_$(ARCH)

UDMA_LIB_OBJS=udma.o udma_mem.o udma_chan.o
UDMA_LIB=libudma

UDMA_TEST_OBJS=udma_test.o
UDMA_TEST=udma_test

all: $(UDMA_LIB).so $(UDMA_LIB).a $(UDMA_TEST)

.PHONY: all build clean distclean

$(UDMA_TEST): $(UDMA_TEST_OBJS) $(UDMA_LIB).a
	$(CC) -g -ggdb2 ${LDFLAGS} -o $@ $^ ./$(UDMA_LIB).a 

$(UDMA_LIB).so:  $(UDMA_LIB_OBJS)
	$(CC) -g -ggdb2 -Wl,-soname=$@.1 -shared -fPIC ${LDFLAGS} -o $@ $^

$(UDMA_LIB).a:  $(UDMA_LIB_OBJS)
	$(AR) ${ARFLAGS} $@ $^

%.o: %.c
	$(CC) -g -ggdb2 ${CFLAGS} ${EXTRA_CFLAGS} -c -o $@ $<

clean: 
	rm -rf $(UDMA_LIB).so $(UDMA_LIB).a $(UDMA_TEST) *.o

distclean: clean

